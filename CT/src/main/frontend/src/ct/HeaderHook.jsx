import React, { useMemo, useState, useEffect, Suspense, lazy } from 'react';
import './ct.css';
import store from '../reduxgrp/store';
import { useDispatch, useSelector } from "react-redux";
import { loginAction } from '../reduxgrp'
import {
BrowserRouter as Router,
        Redirect,
        Switch,
        Route,
        NavLink
        } from "react-router-dom";


import Aboutme from '../container/Aboutme.jsx';

import StockMarket from '../container/Stock.jsx';
import Login from '../container/Login.jsx';


function HeaderHook(props) {

    const [loadOnStartUp, setLoadOnStartUp] = useState(true);
    const [lgName, setLgName] = useState('login');
    const reduxFdata = useSelector((login) => ({fdata: login.fdata, status: login.status, vdata: login.vdata, user: login.user})) //this is inplace of mapStatetoProps in redux
    const dispatch = useDispatch();
    
    const headerEffect = useEffect(
            () => {
        var storeval = store.getState()
        const val = (storeval !== undefined && storeval.login !== undefined && storeval.login.status !== undefined && storeval.login.status === 'LoggedIn') ? setLgName('Logout') : setLgName('Login')

    }
    , [props]);


    const handleOnClick = (event) => {
        if (event === undefined)
            return;
        setLoadOnStartUp(false);
        if (lgName === 'Logout') {
            var storeval = store.getState()
            const login = (storeval !== undefined && storeval.login !== undefined) ? storeval.login : ''
            login !== '' && dispatch(loginAction({fdata: login.fdata, status: 'Logout', vdata: login.vdata, user: login.user}));
        }
    }

    return (
            <div>
                {(props.update === 'NA') &&
                            <div id='headerone'>
                                <Router>
                                    <div className="topheader" >
                                        <span className="menubox">
                                            <span>  
                                                <NavLink activeStyle={{fontWeight: "bold", background: "aqua", color: "black", fontSize: "10", borderColor: "grey"}}
                                                         to='/aboutme'
                                                         className='menubox'
                                                         name='aboutme'
                                                         onClick={(event) => handleOnClick(event)}>AboutMe</NavLink>
                                            </span><span>
                                                <NavLink activeStyle={{fontWeight: "bold", background: "aqua", color: "black", fontSize: "10", borderColor: "grey"}}
                                                         to='/stock'
                                                         className='menubox'
                                                         name='stock'
                                                         onClick={(event) => handleOnClick(event)}>Stock</NavLink>
                                            </span><span>
                                                <NavLink activeStyle={{fontWeight: "bold", background: "aqua", color: "black", fontSize: "10", borderColor: "grey"}}
                                                         to='/login'
                                                         className='menubox'
                                                         name='login'
                                                         onClick={(event) => handleOnClick(event)}>Login</NavLink></span>
                        
                                        </span>
                                    </div>
                                    <div className="middleheader" >
                                        <span className="middlemenubox">CodingTest</span>
                        
                                    </div>
                                    <div className="topheader" >
                                        <span className="menubox">{(new Date()).toString()}</span>
                                    </div>
                                    <div id="innerbody" >
                                        <Switch children>
                                        {(loadOnStartUp) && <Route path="/"><Aboutme shmain='1' /></Route>}
                                        {(props.shmain !== 1) && <Route path="/aboutme"><Aboutme shmain='1' /></Route>}
                                        {(props.shmain !== 2) && <Route path="/stock"><StockMarket shmain='2' vdata={props.vdata} /></Route>}
                                        {(props.shmain !== 3) && <Route path="/login">{lgName === 'Logout' ? <LogOut /> : <Login shmain='3' />}</Route>}
                                        </Switch>
                                    </div>
                                </Router>
                            </div>
                }
            </div>
            );
}

const LogOut = (props) => {
    return (
            <Redirect to='/stock' />
            )
};

export default HeaderHook;
