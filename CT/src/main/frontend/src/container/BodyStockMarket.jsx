import React, { useState, useEffect, useMemo } from 'react';
import { hasValuesInObjectArray, buildStockData, formFetch, RedirectLP } from '../utils/formUtils';
import './stock.css';
import StockTable from './StockTable.jsx';
import up from './up.ico';
import down from './down.ico';
//background service will be updating cache in addition to event update
const cachedStockData = [];
let url = 'http://localhost:8080/load/getStockDetails';
const totalDisplayStockitems = 4;
let itemGlobalState = 0;
let loaded = false;

function BodyStockMarket(props) {

    let fdata = props.fdata;
    const [items, addItems] = useState();
    const [blockFetch, doBlockFetch] = useState(false);
    const [itemNumber, setItemNumber] = useState(itemGlobalState);
    const [results, setResults] = useState();
    const [response, setResponse] = useState();
    const [status, setStatus] = useState();

    const setGItemNumber = (itemno) => {
        setItemNumber(itemno);
        itemGlobalState = itemno;
    }



    const loadOnStartUp = useEffect(
            () => {
        if (loaded) {
            loaded = false;
        } else if (!blockFetch) {
            setGItemNumber(0);
            const data1oad = buildStockData(cachedStockData, fdata);
            formFetch(url, data1oad, setStatus, setResponse, setResults)
            doBlockFetch(true);
            loaded = true;

        }
    }
    , []);

    const updateStockData = useEffect(
            () => {

        if (loaded) {
            loaded = false;
        } else if (!blockFetch) {
            const data1oad = buildStockData(cachedStockData, fdata);
            if (itemNumber >= cachedStockData.length) {
                formFetch(url, data1oad, setStatus, setResponse, setResults)
                doBlockFetch(true)
            }

        }
    }
    , [itemNumber]);

    const [totalItemsInDb, setTotalItemsInDB] = useState(16);
    var max = cachedStockData.length;

    const clickNext = (event) => {
        try {
            const nextitems = [];

            //when rerender itemnumber = 0
            if (!(itemNumber === 0 && cachedStockData.length === 0)) {
                if (itemNumber >= cachedStockData.length) {
                    ((cachedStockData.length - totalDisplayStockitems) > totalDisplayStockitems) ? setGItemNumber(cachedStockData.length - totalDisplayStockitems) : setGItemNumber(cachedStockData.length)
                    return;
                }

            }

            for (let i = itemNumber, j = 0; (i < cachedStockData.length && j < totalDisplayStockitems); i++, j++)
                nextitems.push(cachedStockData[i]);

            if (nextitems !== null && nextitems !== undefined && nextitems.length > 0)
                addItems(nextitems)

            setGItemNumber(itemNumber >= max ? 0 : itemNumber + totalDisplayStockitems)
        } catch (e) {

        }
    }


    const clickRefresh = (event) => {
        try {
            if (itemNumber <= totalDisplayStockitems)
                return;
            setGItemNumber(0);
            const nextitems = [];
            for (let i = 0; i < totalDisplayStockitems; i++)
                nextitems.push(cachedStockData[i]);

            if (nextitems !== null && nextitems !== undefined && nextitems.length > 0)
                addItems(nextitems)
        } catch (e) {
            //console.log("refresh failed:" + e);
        }
    }

    const clickPrev = (event) => {
        try {
            if (itemNumber <= totalDisplayStockitems || itemNumber - totalDisplayStockitems < 0)
                return;

            const nextitems = [];
            switch (itemNumber) {
                default:
                    for (let i = itemNumber - 1, j = 0; i > 0 && j < totalDisplayStockitems; i--, j++)
                        nextitems.push(cachedStockData[i]);
                    if (itemNumber > 0 && nextitems !== null && nextitems !== undefined && nextitems.length > 0)
                        addItems(nextitems)
            }


            setGItemNumber(itemNumber <= totalDisplayStockitems ? cachedStockData.length : itemNumber - totalDisplayStockitems)
        } catch (e) {

        }
    }

    const resultsChange = useEffect(() => {
        try {
            doBlockFetch(false)

            let cachedStockDataLength = cachedStockData.length;

            if (results === null || results === undefined)
                return

            const stockData = results.stockData;
            const stockDataCount = results.stockDataCount;
            const status = results.status;
            if (stockData === null || stockData === [])
                return;
            if (status === 'OK') {
                setTotalItemsInDB(stockDataCount)

                if (stockData !== undefined && stockData.length !== undefined) {
                    const bzDataParsed = JSON.parse(stockData);
                    //mark the duplicate with noupdates  as delete                    
                    for (let i = 0; i < cachedStockData.length; i++) {
                        for (let j = 0; j < bzDataParsed.length; j++) {
                            if (cachedStockData[i].stockId === bzDataParsed[j].stockId && cachedStockData[i].etime === bzDataParsed[j].etime) {
                                //bzDataParsed.splice(j,1);
                                cachedStockData.splice(i, 1);
                            }
                        }
                    }

                    //cleanup old cache
                    for (let j = 0; j < bzDataParsed.length; j++) {
                        cachedStockData.push(bzDataParsed[j])
                    }

                    //load on startup
                    if (cachedStockDataLength < 1) {
                        const nextitems = [];
                        for (let i = 0; i < totalDisplayStockitems; i++)
                            nextitems.push(cachedStockData[i]);
                        if (nextitems !== null && nextitems !== undefined && nextitems.length > 0)
                            addItems(nextitems)
                    }
                  
                }
            }
        } catch (ex) {
          
        }

    }, [results])

    return (
            <div >
                <h1>Stock Display</h1>
                <span className="scrollbar">
                    <button id="prev1" className="next" onClick={(event) => clickPrev(event)}>&lt;Prev</button>
                    <button id="refresh1" className="next" onClick={(event) => clickRefresh(event)}>Refresh</button>
                    <button id="next1" className="next" onClick={(event) => clickNext(event)}>Next&gt;</button>
                </span>
            
                <div className="spdisplay">
            
                    <span className="spdisplayHeader">Stock</span>
                    <span className="spdisplayHeader">Bid Price</span>
                    <span className="spdisplayHeader">Ask Price </span>
                    <span className="spdisplayHeader">Event Time</span>
                    <span className="spdisplayHeader">Market Price </span>                
                    <span className="spdisplayHeader">up/down</span>
            
                </div>
                {(items !== null && items !== undefined && items.length > 0) && <StockTable itemNumber={itemNumber} items={items}/>}
            </div>
            );

}

export default BodyStockMarket;
