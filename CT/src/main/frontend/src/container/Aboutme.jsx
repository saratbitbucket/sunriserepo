import React, { Component } from 'react';


class Aboutme extends Component {
    constructor() {
        super();
        this.state = {
            selectedheader: 1,
            sessionContent: ""
        }

        this.setStateHandler = this.setStateHandler.bind(this);

    }
    ;
            forceUpdateHandler() {
        this.forceUpdate();
    }
    ;
            setStateHandler() {
        this.setState({selectedheader: this.props.shmain})
    }
    ;
            render() {
        return (
                <div ref="portalref49">
                    <h1>Aboutme</h1>
                    <div><p>
                
                            Frontend:
                            Providing real-time stock pricing display on the web page. It needs to have:
                            1. Stock symbol
                            2. Market price (Mid) (mid = (bid + ask)/2)
                            3. Arrow to show the trend of pricing going up or down from last update.</p></div>
                    <div><p>
                            Backend:
                            Suppose you have a realtime pricing event from Reuters, containing the information of
                            1. Stock symbol
                            2. bid price
                            3. ask price
                            4. event time
                        </p></div> 
                        
                    <div><p>
                            Please come out end to end solution of how you feed the information from backend to the web UI. The solution must be:
                            1. should be executable and the end to end flow is demo-able.
                            2. should have good unit test both on front end and backend code.
                            3. Should have a readme to tell how to run your application.
                            4. The source code should be upload to a private bitbucket link and share to me.
                            5.Try to apply Cloud native best practice.
                
                        </p></div>
                </div>


                );
    }
}

export default Aboutme;
