const CryptoJS = require('crypto-js');


    
const xmlEscUtil = (val) => {
	var rep=new RegExp("([^a-zA-z_0-9])","g");//g-global & use doublequotes
	var valc = (val !== null && val !== undefined) ? val.toString().replace(rep, '-').toString() : " ";
	return valc;
  }  
let ivVal = '5D9r9ZVzEYYgha93/aUK2w==';
let keyCR = 'u/Gu5posvwDsXUnV5Zaq4g==';

export const sysinfo = (sysdata) => {
        sysdata = xmlEscUtil(window.navigator.appName);
        sysdata = sysdata + '-vk-' + xmlEscUtil(window.navigator.appVersion);
        sysdata = sysdata + '-vk-' + xmlEscUtil(window.navigator.buildID);
        sysdata = sysdata + '-vk-' + xmlEscUtil(window.navigator.cookieEnabled);
        sysdata = sysdata + '-vk-' + xmlEscUtil(window.navigator.doNotTrack);
        sysdata = sysdata + '-vk-' + xmlEscUtil(window.navigator.language);
        sysdata = sysdata + '-vk-' + xmlEscUtil(window.navigator.onLine);
        sysdata = sysdata + '-vk-' + xmlEscUtil(window.navigator.oscpu);
        sysdata = sysdata + '-vk-' + xmlEscUtil(window.navigator.platform);
        sysdata = sysdata + '-vk-' + xmlEscUtil(window.navigator.plugins);
        sysdata = sysdata + '-vk-' + xmlEscUtil(window.navigator.product);
        sysdata = sysdata + '-vk-' + xmlEscUtil(window.navigator.productSub);
        sysdata = sysdata + '-vk-' + xmlEscUtil(window.navigator.userAgent);
        sysdata = sysdata + '-vk-' + xmlEscUtil(window.navigator.vendor);
        sysdata = sysdata + '-vk-' + xmlEscUtil(window.navigator.vendorSub);
        sysdata = sysdata + '-vk-' + xmlEscUtil(window.navigator.mimeTypes);
        sysdata = sysdata + '-vk-' + xmlEscUtil(window.navigator.javaEnabled());
        sysdata = sysdata + '-vk-' + xmlEscUtil(new Date()) + '-vk-';
        return new String(doEncryption(sysdata, keyCR, ivVal)).toString();
};
     

const doEncryption = (msg, keyCR, ivVal) => {
        try {
                var encrypted = CryptoJS.enc.Utf8.parse(msg);
                var key = CryptoJS.enc.Base64.parse(keyCR);
                var iv = CryptoJS.enc.Base64.parse(ivVal);
                return CryptoJS.AES.encrypt(
                        encrypted,
                        key,
                        { mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7, iv: iv, });
        } catch(exception) {


        }
        return msg;
}

const doDecryption = (chf, keyCR, ivVal) => {
        try {
                var encrypted = CryptoJS.enc.Base64.parse(chf);
                var key = CryptoJS.enc.Base64.parse(keyCR);
                var iv = CryptoJS.enc.Base64.parse(ivVal);
                return (CryptoJS.enc.Utf8.stringify(CryptoJS.AES.decrypt(
                        { ciphertext: encrypted },
                        key,
                        { mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7, iv: iv, })));
        } catch(exception) {


        }
        return chf;
};
