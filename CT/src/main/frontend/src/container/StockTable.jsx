import React, { useMemo, useState, useEffect, Suspense, lazy } from 'react';
import LazyLoad from 'react-lazyload';
import store from '../reduxgrp/store';

import up from './up.ico';
import down from './down.ico';

export default function StockTable(props) {
    return <StockItem itemNumber={props.itemNumber} items={props.items} />;
}
const StockItem = (pageDetails) => {   
    const itemNumber = pageDetails.itemNumber;   
    const initialState = (pageDetails !== undefined && pageDetails.items !== undefined) ? (pageDetails.items) : [{}];
    const secondaryState = (pageDetails !== undefined && pageDetails.items !== undefined) ? (pageDetails.items) : [{}];

    const [stockState, setStockState] = useState(itemNumber === 1 ? secondaryState : secondaryState);
    const car2 = useEffect(() => {
        setStockState(itemNumber === 1 ? initialState : secondaryState)
    }, [itemNumber])

    return (
        <React.Fragment>
            {
                stockState.map((stockItem) => {
                    return <StockItems {...stockItem} key={stockItem.id} />
                })
            }
        </React.Fragment>
    )
}


export function StockItems(props) {
    const stockItem = props;
    const defaultImage = "url(imageLoading.png)";   
    const imageLz = React.lazy(() => import(defaultImage));

    return (            
        <React.Fragment>         
            <div className="spdisplay"> 
            <span className="spdisplayData">{stockItem.ico}</span>                
            <span className="spdisplayData">{stockItem.bidPrice}</span>
            <span className="spdisplayData">{stockItem.askPrice}</span>
            <span className="spdisplayData">{stockItem.etime}</span>
            <span className="spdisplayData">{stockItem.marketPrice}</span>
            <span className="spdisplayData"> {(stockItem.trend===false)?<img src={up} alt="up" height="50%" width="10%"/>:<img src={down} alt="down" height="50%" width="10%"/>}</span>
            </div>
        </React.Fragment>
    )

}