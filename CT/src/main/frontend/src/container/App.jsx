import React, { Component } from 'react';
import './App.css';
import HeaderHook from '../ct/HeaderHook.jsx';
import { connect } from 'react-redux';
import { USER } from './LoginActionTypes.js';
import { loginAction } from '../reduxgrp'
const cutil = require('./cutil.jsx');
const sysInfodata =  cutil.sysinfo('');

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {          
            systemData: '',
            fdata:''
           
        }
        this.systemData = this.systemData.bind(this);
    };

    systemData() {
        this.setState({ systemData: cutil.sysinfo('') })
    };
    
    componentDidMount() {
            this.props.loginAction({fdata:sysInfodata});         
            this.setState({ systemData:  sysInfodata}); 
    }

    render() {
        return (
            <div ref="portalref1">

                {((this.props.vdata === undefined || this.props.vdata === '' || this.props.vdata === null || this.props.vdata === 'null') ?
                    <HeaderHook shmain={this.props.shmain} update='NA' udata='new' vdata={this.state.systemData} /> :
                    <HeaderHook shmain={this.props.shmain} update='NA' udata='new' vdata={this.state.systemData} />)
                }

            </div>

        );
    }
}


    const mapStateToProps = state => state.login

    const mapDispatchToProps = dispatch => {      
        return {
		loginAction: fdata => dispatch(loginAction(fdata))
        }
    }


    export default connect(mapStateToProps, mapDispatchToProps)(App);