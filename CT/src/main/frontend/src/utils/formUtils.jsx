import React from 'react';
import { Redirect } from "react-router-dom";
import FileSaver from 'file-saver';
const CryptoJS = require('crypto-js');
let ivVal = '5D9r9ZVzEYYgha93/aUK2w==';
let keyCR = 'u/Gu5posvwDsXUnV5Zaq4g==';


export function hasValuesInObjectArray(errorTemp) {
    let errorsFound = false;
    let val = Object.values(errorTemp);
    val.forEach((v) => {
        if (errorsFound)
            return;
        if (v !== "") {
            errorsFound = true;
            return;
        }
    });
    return errorsFound;
}

export function doEncryption(msg, keyCR, ivVal) {
    try {
    var encrypted = CryptoJS.enc.Utf8.parse(msg);
    var key = CryptoJS.enc.Base64.parse(keyCR);
            var iv = CryptoJS.enc.Base64.parse(ivVal);
            return CryptoJS.AES.encrypt(encrypted, key, { mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7, iv: iv, });
    }
    catch (e){ }
    return msg;
}
;



export const RedirectLP = (props) => (props.status === 'LoggedIn' && < div > < Redirect to={
        {
            pathname: '/stock',
            state: {
                vdata: props.sessionData
            },
        }
}
/></div >);

export function formFetch(url, data, setStatus, setResponse, setResults) {
    console.log("Fetch is called....")
    let fetchError = Object,
            response = Object,
            results = Object;
    {
        //console.log(JSON.stringify(data));
        fetch(url, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: 'POST',
            body: JSON.stringify(data)
        }).then((Response) => {
            response = Response.json();
            setResponse(Response)
            return response;
        }).then((Result) => {
            setResults(Result)
            return results = Result;
        }).catch(errors => {
            console.log("Failed: Try again,Server busy....");
            fetchError = errors;
            setStatus("Failed: Try again,Server busy....");
        });
    }
    return {response, results, fetchError};
}


export function buildLoginData(email, password, username, vdata) {

    return {
        customerId: username,
        txMetaData: {
            "urldata": "",
            "fdata": new String(doEncryption(vdata, keyCR, ivVal)).toString(),
            "type": ""

        },
        txBzData: {
            bzData: new String(doEncryption(JSON.stringify({
                "email": new String(doEncryption(email, keyCR, ivVal)).toString(),
                "password": new String(doEncryption(password, keyCR, ivVal)).toString(),
                "customerId": username
            }), keyCR, ivVal)).toString()
        }
    }
}


export function buildStockData(preloaded, fdata) {
    let avbl = [];
    for (let i = 0; i < preloaded.length; i++) {
        avbl.push(preloaded[i].id);
    }
    return {
        customerId: "temp-" + (new Date()).getTime(),       
        stockData: new String(doEncryption(JSON.stringify({
            "customerId": "temp-" + (new Date()).getTime(),            
            "status": fdata !== undefined && fdata.status !== undefined && fdata.status,
            "avbl": new String(doEncryption(avbl, keyCR, ivVal)).toString()
        }), keyCR, ivVal)).toString()
    }
}





