import { USER} from './../container/LoginActionTypes.js';


export const loginAction = (login) => { 
    return {
        type: USER,
        payload: buildLoginActionData(login)
    };
};

const buildLoginActionData = (login) => { 
    return {fdata: login.fdata, vdata: login.vdata, status: login.status, user: login.user};
}