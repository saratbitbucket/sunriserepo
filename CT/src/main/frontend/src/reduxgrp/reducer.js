import { USER} from './../container/LoginActionTypes.js';

const initialState = {fdata: '', vdata: '', status: '', user: ''};

export default function (state = initialState, action) {
    
    switch (action.type) {
        case USER:
            return {
                ...state,
                fdata: (action.payload !== undefined && action.payload.fdata !== undefined) ? action.payload.fdata : state.fdata,
                status: (action.payload !== undefined && action.payload.status !== undefined) ? action.payload.status : state.status,
                vdata: (action.payload !== undefined && action.payload.vdata !== undefined) ? action.payload.vdata : state.vdata,
                user: (action.payload !== undefined && action.payload.user !== undefined) ? action.payload.user : state.user
            }
        default:
            return state;
}
}

