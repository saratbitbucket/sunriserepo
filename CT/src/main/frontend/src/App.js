import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import ProxyRouter from './container/ProxyRouter.jsx';

ReactDOM.render(<ProxyRouter /> , document.getElementById('root'));


