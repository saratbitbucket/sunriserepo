package com.ct.service;

import com.ct.data.model.CTModel;
import com.ct.data.model.StockData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.ct.model.VMInternal;
import com.generic.model.util.CTUtil;

@Service
public class CTService {

    private static final Logger log = LoggerFactory.getLogger(CTService.class);

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    public CTService() {

    }

    public VMInternal checkReliability(VMInternal ctModel) {
        return ctModel;
    }

    public boolean isReliable(CTModel ctModel) {
        return true;
    }

    //Todo: Use jaksonparser API instead
    private StockData unMarshallStockData(String stockDataEnc) {
        StockData sd = new StockData();
        String stockDataDec = CTUtil.decryptCli(stockDataEnc, null, null);
        log.info("stockDataDec=" + stockDataDec);
        //add json parser for multi dimentional
        String[] split = stockDataDec.split(("\\,|\\}|\\{"));
        for (String bzItem : split) {
            final String[] key = bzItem.split(":");
            if (key.length < 2) {
                continue;
            }
            final String keyBz = key[0].replace("\"", "").trim();
            final String valueBz = key[1].trim();

            switch (keyBz) {
                case "stockId":
                    sd.setStockId(valueBz);
                    break;
                case "bidPrice":
                    sd.setBidPrice(Float.valueOf(valueBz));
                    break;
                case "askPrice":
                    sd.setAskPrice(Float.valueOf(valueBz));
                    break;
                case "marketPrice":
                    sd.setMarketPrice(Float.valueOf(valueBz));
                    break;
                case "ico":
                    sd.setIco(valueBz);
                    break;
                case "fdata":
                    sd.setFdata(valueBz);
                    break;

            }
        }

        return sd;
    }

    public ResponseEntity<CTModel> routeRequest(CTModel ctModel, String pg) {
        try {
            //String stockDataEnc = ctModel.getStockData();
            // StockData sd = unMarshallStockData(stockDataEnc);

            //should be routed to different microservices
            //exchange to sercives code here
            //..............
            //For now only one service and static data
            //Need to do unmarshalling due to time constraint just spliting it stockDataDec
            //----------------------------------------------------------------------//
            //********************* Stock Reuters**********************************//
            //---------------------------------------------------------------------//
            String staticBzData = getStaticTestData(ctModel);
            ctModel.setStockData(staticBzData);
            return new ResponseEntity<>(ctModel, HttpStatus.OK);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

public float getPrevMarketPrice(){

//should be queried form other service
//below is test data
long l = System.currentTimeMillis();
        int L2 = ((int) (l % 100));
return L2;
}

    public String getStaticTestData(CTModel ctModel) {
        long l = System.currentTimeMillis();
        int L2 = ((int) (l % 100));
        String bzData = "[";
        int max = 16;
        for (int i = 0; i < max; i++) {
            StockData sd = new StockData();
            sd.setStockId("abc-" + L2 + "-" + i);
            sd.setAskPrice(2010 + i);
            sd.setBidPrice(1010 + i);
            sd.setIco("Icon" + L2 + i);
            sd.setMarketPrice((sd.getAskPrice() + sd.getBidPrice()) / 2);
            sd.setEtime(l + i);
            sd.setTrend((getPrevMarketPrice()-sd.getMarketPrice())>0);
            bzData = bzData + sd.toString() + ((i == max - 1) ? "]" : ",");
        }
        log.info("stockdata" + bzData);
        ctModel.setStockDataCount(max-1);
        ctModel.setStatus("OK");        
        return bzData;
    }

}
