package com.ct.controller;

import com.ct.data.model.CTModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import com.ct.service.CTService;

@RestController
public class CTController {

    private static final Logger log = LoggerFactory.getLogger(CTController.class);
    private final CTService vService;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    public CTController(CTService vService) {
        this.vService = vService;
    }

    @RequestMapping(value = "/{pg}/getStockDetails", method = RequestMethod.POST, produces = "application/json")
    public CTModel process(@RequestBody CTModel ctModel, @PathVariable String pg) {
        try {
            log.info("getStockDetails Request : Started .....");
            log.info(ctModel.toString());
            if (vService.isReliable(ctModel)) {
                ResponseEntity<CTModel> re = vService.routeRequest(ctModel, pg);
                if (re.getBody() != null) {
                    System.out.println(re.getBody().toString());
                }
                final CTModel ctModelResponse = (CTModel) re.getBody();
                if (HttpStatus.OK == re.getStatusCode()) {
                    ctModel.setStatus("OK");
                    return ctModelResponse;
                } else if (HttpStatus.ACCEPTED == re.getStatusCode() && re.getBody() != null) {
                    ctModel.setStatus("OK");
                    return ctModelResponse;
                }

            } else {
                ctModel.setStatus("Reliability Check Failed");
                return ctModel;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            ctModel.setStatus("FAILED");
            return ctModel;
        }
        ctModel.setStatus("UNREACHABLE");
        return ctModel;
    }

}
