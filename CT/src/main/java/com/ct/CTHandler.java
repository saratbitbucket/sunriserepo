package com.ct;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class CTHandler extends SpringBootServletInitializer {


	private static final Logger log = LoggerFactory.getLogger(CTHandler.class);

	
	private void SpringApplicationWebApplicationInitializer() {
		// TODO Auto-generated method stub

	}

	public static void main(String[] args) {
		SpringApplication.run(CTHandler.class, args);
	}
	
	@Bean
	public RestTemplate getRestTemplate() {
	      return new RestTemplate();
	}

	
}
