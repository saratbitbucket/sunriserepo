package com.generic.model.util;

import com.ct.data.model.BrowserData;
import java.security.Key;
import java.security.spec.AlgorithmParameterSpec;
import org.apache.commons.codec.binary.Base64;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import sun.misc.*;

public class CTUtil {

    public static String getEncodedValue(String value, String type) {
        switch (type) {
            case "fdata":
                return getFdataCode(value);
        }
        return value;
    }

    private static String getFdataCode(String value) {

        String[] encodedArray = value.split("-");
        String encodedValue = "";
        for (int i = 0; i <= encodedArray.length; i++) {
            String temp = encodedArray[i];
            if (temp == null || temp.isEmpty()) {
                continue;
            }
            BrowserData bd = find("_" + temp);

            if (bd == null) {
                bd = find("_" + temp.toUpperCase());
            }
            if (bd != null) {
                encodedValue = encodedValue + bd.getEncodedString();
            } else {
                encodedValue = encodedValue + "-" + temp + "-";
            }

        }

        return getCompressed(encodedValue.replace("--", "-"));
    }

    private static String getCompressed(String val) {

        return val;
    }

    public static BrowserData find(String temp) {
        BrowserData[] values = BrowserData.values();
        for (BrowserData bd : values) {
            if (bd.name().toUpperCase().equalsIgnoreCase(temp)) {
                return bd;
            }
        }
        return null;
    }

    static class Base64ServerEnc {
        //Issue: working only in server side.Failing JavaScript decryption

        private static final String keyStd = "PBEWithMD5AndDES";
        private static final String ALGO = "AES";
        static byte[] keyValue = keyStd.getBytes();

        public static String encrypt(String data, String keyJS) throws Exception {
            Key key = generateKey(keyJS);
            System.out.println(key.getAlgorithm());
            Cipher c = Cipher.getInstance(ALGO);
            int maxKeyLen = Cipher.getMaxAllowedKeyLength("AES");
            System.out.println("MaxAllowedKeyLength=[" + maxKeyLen + "].");
            c.init(Cipher.ENCRYPT_MODE, key);
            byte[] encVal = c.doFinal(data.getBytes("UTF-8"));
            //byte[] encVal = c.doFinal(data.getBytes("ISO-8859-1"));
            return java.util.Base64.getEncoder().encodeToString(encVal);
        }

        public static String decrypt(String encryptedData, String keyJS) throws Exception {
            System.out.println(keyJS + "=<keyJS>");
            Key key = generateKey(keyJS);
            Cipher c = Cipher.getInstance(ALGO);
            c.init(Cipher.DECRYPT_MODE, key);
            @SuppressWarnings("restriction")
            byte[] decordedValue = new BASE64Decoder().decodeBuffer(encryptedData);
            byte[] decValue = c.doFinal(decordedValue);
            String decryptedValue = new String(decValue);
            return decryptedValue;
        }

        private static Key generateKey(String keyJS) throws Exception {
            return keyJS != null ? new SecretKeySpec(keyJS.getBytes(), ALGO) : new SecretKeySpec(keyValue, ALGO);

        }

    }

    static class Base64ClientEnc {

        private static final String keyStd = "PBEWithMD5AndDES";
        private static final String ALGO = "AES";
        static byte[] keyValue = keyStd.getBytes();

        public static String encryptJS(String msg, String keyV, String ivVal) {
            try {
                SecretKey key = new SecretKeySpec(
                        Base64.decodeBase64(keyV), "AES");
                AlgorithmParameterSpec iv = new IvParameterSpec(
                        Base64.decodeBase64(ivVal));
                Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
                cipher.init(Cipher.ENCRYPT_MODE, key, iv);
                return (Base64.encodeBase64String(cipher.doFinal(
                        msg.getBytes("UTF-8"))));
            } catch (Exception ex) {

            }
            return msg;
        }

        public static String decryptJS(String encrptedText, String keyV, String ivVal) {
            try {
                SecretKey key = new SecretKeySpec(
                        Base64.decodeBase64(keyV), "AES");
                AlgorithmParameterSpec iv = new IvParameterSpec(
                        Base64.decodeBase64(ivVal));
                Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
                cipher.init(Cipher.DECRYPT_MODE, key, iv);
                byte[] decordedValue = Base64.decodeBase64(encrptedText);
                byte[] decValue = cipher.doFinal(decordedValue);
                String decryptedValue = new String(decValue);
                return (new String(decryptedValue.getBytes()).toString());
            } catch (Exception ex) {

            }
            return encrptedText;
        }
    }

    public static void main(String args[]) {

        String av = "appName-Netscape-appVersion-5-0--Windows-NT-10-0--Win64--x64--AppleWebKit-537-36--KHTML--like-Gecko--Chrome-78-0-3904-97-Safari-537-36-buildID-undefined-cookieEnabled-true-doNotTrack-null-language-en-GB-onLine-false-oscpu-undefined-platform-Win32-plugins--object-PluginArray--product-Gecko-productSub-20030107-userAgent-Mozilla-5-0--Windows-NT-10-0--Win64--x64--AppleWebKit-537-36--KHTML--like-Gecko--Chrome-78-0-3904-97-Safari-537-36-vendor-Google-Inc--vendorSub--mimeTypes--object-MimeTypeArray--javaEnabled-false-date-Tue-Dec-24-2019-18-26-23-GMT-0000--Greenwich-Mean-Time-";
       
        String bv = "U2FsdGVkX1+ViZZboV7eeDgMHJGMBTO11CFpKaveDzGuAntXzX…8/rVifZzFJ5OgRAv/iwVPoqWJ9AHGxNB4cgjHI1AhXekWE+A=";
        String keyStd = "PBEWithMD5AndDES";
        av = "pandit";
        String decrpptSample1 = "MQiIQXTLMgSAxIyU3/nMwQ==";
        String decrpptSample2 = "U2FsdGVkX1+X4mGVjvJRdTJoSi6iXyeoJ3zQHCXB32U==";
        try {
            System.out.println("source=" + av);
            String statusEnc = encryptCli(av, null, null);
            System.out.println("encryption result=" + statusEnc + "<length>=" + statusEnc.length());
            String statusDec = decryptCli(decrpptSample1, null, null);
            System.out.println("decryption result=" + statusDec);

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static String decryptCli(String value, String keyV, String ivVal) {
        try {
            String key = (keyV != null) ? keyV : "u/Gu5posvwDsXUnV5Zaq4g==";
            String ivVl = (ivVal != null) ? ivVal : "5D9r9ZVzEYYgha93/aUK2w==";
            return Base64ClientEnc.decryptJS(value, key, ivVl);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    public static String encryptCli(String value, String keyV, String ivVal) {
        try {
            String key = (keyV != null) ? keyV : "u/Gu5posvwDsXUnV5Zaq4g==";
            String ivVl = (ivVal != null) ? ivVal : "5D9r9ZVzEYYgha93/aUK2w==";
            return Base64ClientEnc.encryptJS(value, key, ivVl);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    public static String decryptSvr(String value, String keyJS) {
        try {
            return Base64ServerEnc.decrypt(value, keyJS != null ? keyJS : Base64ServerEnc.keyStd);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return value;
    }

    public static String encryptSvr(String value, String keyJS) {
        try {
            return Base64ServerEnc.encrypt(value, keyJS != null ? keyJS : Base64ServerEnc.keyStd);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return value;
    }
    
    
    

}
