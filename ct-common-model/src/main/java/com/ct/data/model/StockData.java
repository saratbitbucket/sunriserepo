package com.ct.data.model;

public class StockData {

    private String stockId;
    private float bidPrice;
    private float askPrice;
    private float marketPrice;
    private String ico;
    private long etime;
    private boolean trend;
    private String fdata;

    public StockData() {
      
    }

    public StockData(String stockId, float bidPrice, float askPrice, float marketPrice, String ico, String fdata,boolean trend) {
        this.stockId = stockId;
        this.bidPrice = bidPrice;
        this.askPrice = askPrice;
        this.marketPrice = marketPrice;
        this.ico = ico;
        this.fdata = fdata;
        this.trend=trend;
    }

    public String getStockId() {
        return stockId;
    }

    public void setStockId(String stockId) {
        this.stockId = stockId;
    }

    public float getBidPrice() {
        return bidPrice;
    }

    public void setBidPrice(float bidPrice) {
        this.bidPrice = bidPrice;
    }

    public float getAskPrice() {
        return askPrice;
    }

    public void setAskPrice(float askPrice) {
        this.askPrice = askPrice;
    }

    public float getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(float marketPrice) {
        this.marketPrice = marketPrice;
    }

    public String getIco() {
        return ico;
    }

    public void setIco(String ico) {
        this.ico = ico;
    }

    public boolean isTrend() {
        return trend;
    }

    public void setTrend(boolean trend) {
        this.trend = trend;
    }
    

    public String getFdata() {
        return fdata;
    }

    public void setFdata(String fdata) {
        this.fdata = fdata;
    }

    public long getEtime() {
        return etime;
    }

    public void setEtime(long etime) {
        this.etime = etime;
    }

    @Override
    public String toString() {
        return "{" + "\"stockId\":\""+ stockId + "\", \"bidPrice\":\"" + bidPrice + "\", \"askPrice\":\"" + askPrice + "\", \"marketPrice\":\"" + marketPrice + "\", \"ico\":\"" + ico +  "\", \"etime\":\"" + etime +  "\", \"trend\":" + trend + ", \"fdata\":\"" + fdata + "\"}";
    }

}
