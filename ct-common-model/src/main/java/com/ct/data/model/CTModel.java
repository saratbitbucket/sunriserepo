package com.ct.data.model;

public class CTModel {

    private String customerID;
    private String stockId;
    private String fdata;
    private String status;
    private String stockData;
    private int stockDataCount;

    public CTModel() {
    }

    public CTModel(String customerID, String stockId, String fdata, String status, String stockData) {
        this.customerID = customerID;
        this.stockId = stockId;
        this.fdata = fdata;
        this.status = status;
        this.stockData = stockData;
    }

    public String getCustomerID() {
        return customerID;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    public String getStockId() {
        return stockId;
    }

    public void setStockId(String stockId) {
        this.stockId = stockId;
    }

    public String getFdata() {
        return fdata;
    }

    public void setFdata(String fdata) {
        this.fdata = fdata;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStockData() {
        return stockData;
    }

    public void setStockData(String stockData) {
        this.stockData = stockData;
    }

    public int getStockDataCount() {
        return stockDataCount;
    }

    public void setStockDataCount(int stockDataCount) {
        this.stockDataCount = stockDataCount;
    }
    
    

    @Override
    public String toString() {
        return "{\"" + "\"customerID\":\"" + customerID + "\", \"stockId\":\"" + stockId + "\", \"fdata\":\"" + fdata + "\", \"status\":\"" + status + "\", \"stockData\":\"" + stockData +"\", \"stockDataCount\":\"" + stockDataCount + "\"}";
    }

}
