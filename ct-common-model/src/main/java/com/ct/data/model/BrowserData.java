package com.ct.data.model;
/*
 * Security risk:this should be moved to webservice

*/

public enum BrowserData {
    _appName(1, 'k'),
    _appVersion(2, 'k'),
    _buildID(3, 'k'),
    _cookieEnabled(4, 'k'),
    _doNotTrack(5, 'k'),
    _language(6, 'k'),
    _onLine(7, 'k'),
    _oscpu(8, 'k'),
    _platform(9, 'k'),
    _plugins(10, 'k'),
    _product(11, 'k'),
    _productSub(12, 'k'),
    _userAgent(13, 'k'),
    _vendor(14, 'k'),
    _vendorSub(15, 'k'),
    _mimeTypes(16, 'k'),
    _javaEnabled(17, 'k'),
    _date(18, 'k'),
    _Netscape(1, 'v'),
    _Windows(2, 'v'),
    _AppleWebKit(3, 'v'),
    _KHTMLlike(4, 'v'),
    _Gecko(5, 'v'),
    _Chrome(6, 'v'),
    _Safari(7, 'v'),
    _undefined(8, 'v'),
    _true(9,'v'),
    _false(10,'v'),
    _Win32(11,'v'),
    _Win64(12,'v'),
    _object(13,'v'),
    _PluginArray(14,'v'),  
    _20030107(15,'v'),
    _Mozilla(16,'v'),
    _Google(17,'v'),
    _MimeTypeArray(18,'v'),
    _NT(19,'v'),
    _like(20,'v'),
    _Greenwich(21,'v'),
    _Mean(22,'v'),
    _Time(23,'v'),
    _null(24,'v'),
    _KHTML(25,'v'),
    _GMT(26,'v'),
    _Mon(27,'v'),
    _Tue(28,'v'),
    _Wed(29,'v'),
    _Thu(30,'v'),
    _Fri(31,'v'),
    _Sat(32,'v'),
    _Sun(33,'v'),
    _Jan(34,'v'),
    _Feb(35,'v'),
    _Mar(36,'v'),
    _Apr(37,'v'),
    _May(38,'v'),
    _Jun(39,'v'),
    _Jul(40,'v'),
    _Aug(41,'v'),
    _Sep(42,'v'),
    _Oct(43,'v'),
    _Nov(44,'v'),
    _Sec(45,'v'),
    _Dec(46,'v'),
    _Standard(47,'v'),
    _Inc(48,'v'),
    _Edge(49,'v'),
    _Internet(50,'v'),
    _explore(51,'v'),
    _opera(51,'v');
	
	int code ;char type;
	
	
	BrowserData(int code ,char type){
	   this.code= code;this.type=type;
     }
	
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public char getType() {
		return type;
	}
	public void setType(char type) {
		this.type = type;
	}
	
	public String getEncodedString() {
		return "-"+getType()+getCode()+"-";
	}
        
        
	
	 
}
